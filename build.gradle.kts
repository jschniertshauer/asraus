plugins {
    kotlin("jvm") version "1.3.61"
    `maven-publish`
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testCompile("org.junit.jupiter:junit-jupiter-api:5.2.2")
}


tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"

    }


    test {
        useJUnitPlatform()
        // ...
        systemProperties = mapOf(
            "junit.jupiter.extensions.autodetection.enabled" to "true",
            "junit.jupiter.testinstance.lifecycle.default" to "per_class")
        // ...
    }
}
