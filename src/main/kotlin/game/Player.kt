package game
class Hand (val deck:CardDeck) {
    val cards = mutableListOf<Card>()

    init {
        repeat(5) {
            cards.add(deck.draw())
        }
    }

    fun refill(){
        while(cards.size < 5) {
            cards.add(deck.draw())
        }
    }

    fun use(stack: PlayStack, idx:Int) {
        val card = cards[idx]
        if(stack.canPush(card)) {
            stack.push(card)
            cards.remove(card)
        }
    }

    override  fun toString(): String  =
        cards.withIndex().map { (index, card) -> "$index=$card" }
        .joinToString(prefix = "Hand [", postfix = "]")
}

class Ablage{
    private val cards = mutableListOf<Card>()

    fun push(c:Card) {
        cards.add(c)
    }

    fun use(stack:PlayStack) {
        if(cards.isEmpty()) return
        val card = cards.last()
        if(stack.canPush(card)) {
            stack.push(card)
            cards.remove(card)
        }
    }

    fun canPush(stack:PlayStack):Boolean {
        if(cards.isEmpty()) return false

        val card = cards.last()
        if(stack.canPush(card)) {
            return true
        }
        return false
    }

    override fun toString(): String {
        return cards.reversed().joinToString (prefix = "Ablage [", postfix = "]")
    }
}

class Player(deck:CardDeck) {
    val spielStapel = PlayerStack(deck)
    val hand = Hand(deck)
    val ablagen = Array<Ablage>(4) {Ablage()}

    fun hasWon() = spielStapel.isEmpty()

    override fun toString(): String {
        return spielStapel.toString() + "\n" + hand.toString() + "\n" + ablagen.joinToString(separator = "\n")
    }

    fun playHand(cardIdx: Int, stack:PlayStack) {
        hand.use(stack, cardIdx)

    }

    fun placeHand(cardIdx: Int, ablageIdx: Int):Boolean {
        ablagen[ablageIdx].push(hand.cards.removeAt(cardIdx))
        hand.refill()
        return false
    }

    fun playAblage(ablageIdx: Int, stack:PlayStack) {
        ablagen[ablageIdx].use(stack)
    }

    fun createStack(cardIdx: Int): PlayStack {
        return PlayStack(hand.cards.removeAt(cardIdx))
    }

    fun playStack(playStack: PlayStack) {
        spielStapel.use(playStack)
    }
}