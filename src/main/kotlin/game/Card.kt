package game


data class Card (val symbol: Symbol, val value:Value) {

    enum class Symbol {
        HEARTS, CLUBS, DIAMONDS, SPADES;
    }

    enum class Value(val display:String, val points:Int) {
        ACE("A", 1),
        TWO("2", 2),
        THREE("3", 3),
        FOUR("4", 4),
        FIVE("5", 5),
        SIX("6", 6),
        SEVEN("7", 7),
        EIGHT("8", 8),
        NINE("9", 9),
        TEN("10", 10),
        JACK("J", 11),
        QUEEN("Q", 12),
        KING("K", 13)
    }

    override fun toString(): String {
       // return value.toString()
        return when(symbol) {
            Symbol.SPADES -> when(value) {
                Value.ACE   ->"\uD83C\uDCA1"
                Value.TWO   ->"\uD83C\uDCA2"
                Value.THREE ->"\uD83C\uDCA3"
                Value.FOUR  ->"\uD83C\uDCA4"
                Value.FIVE  ->"\uD83C\uDCA5"
                Value.SIX   ->"\uD83C\uDCA6"
                Value.SEVEN ->"\uD83C\uDCA7"
                Value.EIGHT ->"\uD83C\uDCA8"
                Value.NINE  ->"\uD83C\uDCA9"
                Value.TEN   ->"\uD83C\uDCAA"
                Value.JACK  ->"\uD83C\uDCAB"
                Value.QUEEN ->"\uD83C\uDCAC"
                Value.KING  ->"\uD83C\uDCAD"
            }
            Symbol.HEARTS -> when(value) {
                Value.ACE   ->"\uD83C\uDCB1"
                Value.TWO   ->"\uD83C\uDCB2"
                Value.THREE ->"\uD83C\uDCB3"
                Value.FOUR  ->"\uD83C\uDCB4"
                Value.FIVE  ->"\uD83C\uDCB5"
                Value.SIX   ->"\uD83C\uDCB6"
                Value.SEVEN ->"\uD83C\uDCB7"
                Value.EIGHT ->"\uD83C\uDCB8"
                Value.NINE  ->"\uD83C\uDCB9"
                Value.TEN   ->"\uD83C\uDCBA"
                Value.JACK  ->"\uD83C\uDCBB"
                Value.QUEEN ->"\uD83C\uDCBC"
                Value.KING  ->"\uD83C\uDCBD"
            }
            Symbol.DIAMONDS -> when(value) {
                Value.ACE   ->"\uD83C\uDCC1"
                Value.TWO   ->"\uD83C\uDCC2"
                Value.THREE ->"\uD83C\uDCC3"
                Value.FOUR  ->"\uD83C\uDCC4"
                Value.FIVE  ->"\uD83C\uDCC5"
                Value.SIX   ->"\uD83C\uDCC6"
                Value.SEVEN ->"\uD83C\uDCC7"
                Value.EIGHT ->"\uD83C\uDCC8"
                Value.NINE  ->"\uD83C\uDCC9"
                Value.TEN   ->"\uD83C\uDCCA"
                Value.JACK  ->"\uD83C\uDCCB"
                Value.QUEEN ->"\uD83C\uDCCC"
                Value.KING  ->"\uD83C\uDCCD"
            }
            Symbol.CLUBS -> when(value) {
                Value.ACE   ->"\uD83C\uDCD1"
                Value.TWO   ->"\uD83C\uDCD2"
                Value.THREE ->"\uD83C\uDCD3"
                Value.FOUR  ->"\uD83C\uDCD4"
                Value.FIVE  ->"\uD83C\uDCD5"
                Value.SIX   ->"\uD83C\uDCD6"
                Value.SEVEN ->"\uD83C\uDCD7"
                Value.EIGHT ->"\uD83C\uDCD8"
                Value.NINE  ->"\uD83C\uDCD9"
                Value.TEN   ->"\uD83C\uDCDA"
                Value.JACK  ->"\uD83C\uDCDB"
                Value.QUEEN ->"\uD83C\uDCDC"
                Value.KING  ->"\uD83C\uDCDD"
            }
        }
    }
}