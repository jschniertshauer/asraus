package game

class AIPlayer(zugStapel: CardDeck, private val playstacks: MutableList<PlayStack>) {

    val internal = Player(zugStapel)

    private var ablageIdx = 0

    fun play() {
        var movePossible = true
        while (movePossible) {
            // play stack card
            val stacksPossible = playstacks.filter { it.canPush(internal.spielStapel.peek()) }
            if (stacksPossible.isNotEmpty()) {
                internal.playStack(stacksPossible.first())
                continue
            }

            // play ablage card
            var backToStart = false
            for (stack in playstacks) {
                for ((index, ablage) in internal.ablagen.withIndex()) {
                    if (ablage.canPush(stack)) {
                        internal.playAblage(index, stack)
                        backToStart = true
                        break
                    }
                }
            }
            if (backToStart) continue

            // play hand card
            for (stack in playstacks) {
                for ((index, card) in internal.hand.cards.withIndex()) {
                    if (stack.canPush(card)) {
                        internal.playHand(index, stack)
                        backToStart = true
                        break
                    }
                }
            }

            if (!backToStart) movePossible = false
        }

        // ablage
        internal.placeHand(0, ++ablageIdx%4)

        println("AI state")
        println(internal.ablagen.joinToString(separator = "\n"))
    }
}
