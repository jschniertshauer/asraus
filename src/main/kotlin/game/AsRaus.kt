package game

import java.lang.IndexOutOfBoundsException
import java.util.function.Predicate

class AsRaus {

    val zugStapel = CardDeck.doubleDeck(true)
    val playstacks = mutableListOf<PlayStack>()

    val player = Player(zugStapel)
    val aiPlayers = mutableListOf<AIPlayer>()


    var running = true

    fun play() {
        playstacks.add(PlayStack(Card(Card.Symbol.HEARTS, Card.Value.ACE)))
        while(running && (!player.hasWon() || aiPlayers.any { it.internal.hasWon() })) {

            aiPlayers.forEach { it.play() }

            playstacks.removeIf(Predicate { it.isDone() })

            var playerActive = true
            while(running && playerActive) {

                println("Game state:")
                println(playstacks.joinToString())

                println("Player state:")
                println(player)
                val input = readLine() ?: break
                val parts = input.split(" ")
                if (parts.isEmpty()) continue

                val action = parts[0]

                try {

                    when (action) {
                        "exit", "EXIT" -> running = false
                        "create", "CREATE" -> playstacks.add(player.createStack(parts[1].toInt()))
                        "hand", "HAND" -> player.playHand(parts[1].toInt(), playstacks[parts[2].toInt()])
                        "ablegen", "ABLEGEN" -> playerActive = player.placeHand(parts[1].toInt(), parts[2].toInt())
                        "ablage", "ABLAGE" -> player.playAblage(parts[1].toInt(), playstacks[parts[2].toInt()])
                        "stack", "STACK" -> player.playStack(playstacks[parts[1].toInt()])
                    }
                } catch (e: IndexOutOfBoundsException) {

                }
                playstacks.removeIf(Predicate { it.isDone() })
            }
        }

        when {
            player.hasWon() -> {
                println("Player has won the game")
            }
            aiPlayers.any { it.internal.hasWon() } -> {
                println("AI has won the game")
            }
            else -> {
                println("Player has terminated the game")
            }
        }
    }

    fun addAI():AsRaus {
        aiPlayers.add(AIPlayer(zugStapel, playstacks))
        return this
    }
}

fun main(){
    AsRaus().addAI().play()
}
