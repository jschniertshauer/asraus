package game

import game.Card.Symbol
import game.Card.Value
import java.lang.IllegalArgumentException
import java.util.*

class CardDeck {
    private val cards:MutableList<Card> = mutableListOf()

    private constructor(numSets:Int, shuffle:Boolean=true) {
        repeat(numSets) {
            for (sym in Symbol.values()) {
                for (cardval in Value.values()) {
                    cards.add(Card(sym, cardval))
                }
            }
        }
        if(shuffle) shuffle()
    }

    companion object Factory {
        fun emptyDeck() = CardDeck(0)
        fun singleDeck(shuffle: Boolean=true) = CardDeck(1, shuffle)
        fun doubleDeck(shuffle: Boolean=true) = CardDeck(2, shuffle)
        fun tripleDeck(shuffle: Boolean=true) = CardDeck(3, shuffle)

        fun createDeck(numPlayers:Int): CardDeck {
            return when(numPlayers) {
                in 0..1 -> singleDeck()
                in 2..3 -> doubleDeck()
                else -> tripleDeck()
            }
        }
    }

    fun shuffle() = cards.shuffle()

    fun isEmpty() = cards.isEmpty()

    fun draw() = cards.removeAt(0)
}

fun Card.isNext(other:Card): Boolean {
    return value.points == other.value.points-1
}

class PlayStack {
    private var top:Card

    constructor(card:Card) {
        assert(card.value.points == 0)
        top = card
    }

    fun canPush(card:Card):Boolean {
        return top.isNext(card)
    }

    fun push(card:Card) {
        if(!canPush(card)) {
            throw IllegalArgumentException("Card must follow)")
        }
        top = card
    }

    override fun toString(): String {
        return top.value.toString()
    }

    fun isDone(): Boolean {
        return top.value == Card.Value.KING
    }
}

class PlayerStack {
    private val cards = Stack<Card>()

    constructor(deck:CardDeck, num:Int=10) {
        repeat(num) {
            cards.add(deck.draw())
        }
    }

    fun peek() = cards.peek()
    fun use(target:PlayStack) {
        if(target.canPush(peek())) {
            target.push(cards.pop())
        }
    }

    override fun toString(): String {
        return "Spielerstapel: " + peek().value
    }

    fun isEmpty(): Boolean {
        return cards.isEmpty()
    }
}