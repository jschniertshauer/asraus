package game

import game.Card.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CardTest {

    @Test
    fun testIsNext(){
        val aceOfSpades = Card(Symbol.SPADES, Value.ACE)
        // symbol is irrelevant for next
        assertTrue(aceOfSpades.isNext(Card(Symbol.HEARTS, Value.TWO)))
        assertTrue(aceOfSpades.isNext(Card(Symbol.DIAMONDS, Value.TWO)))
        assertTrue(aceOfSpades.isNext(Card(Symbol.CLUBS, Value.TWO)))
        assertTrue(aceOfSpades.isNext(Card(Symbol.SPADES, Value.TWO)))

        // value is relevant for next
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.THREE)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.FOUR)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.FIVE)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.SIX)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.SEVEN)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.EIGHT)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.NINE)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.TEN)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.JACK)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.QUEEN)))
        assertFalse(aceOfSpades.isNext(Card(Symbol.SPADES, Value.KING)))

        val twoOfSpades = Card(Symbol.SPADES, Value.TWO)
        assertFalse(twoOfSpades.isNext(aceOfSpades))
    }

    @Test
    fun testSymbol(){
        assertEquals("\uD83C\uDCA1", Card(Symbol.SPADES, Value.ACE).toString())
        assertEquals("\uD83C\uDCA2", Card(Symbol.SPADES, Value.TWO).toString())
        assertEquals("\uD83C\uDCA3", Card(Symbol.SPADES, Value.THREE).toString())
        assertEquals("\uD83C\uDCA4", Card(Symbol.SPADES, Value.FOUR).toString())
        assertEquals("\uD83C\uDCA5", Card(Symbol.SPADES, Value.FIVE).toString())
        assertEquals("\uD83C\uDCA6", Card(Symbol.SPADES, Value.SIX).toString())
        assertEquals("\uD83C\uDCA7", Card(Symbol.SPADES, Value.SEVEN).toString())
        assertEquals("\uD83C\uDCA8", Card(Symbol.SPADES, Value.EIGHT).toString())
        assertEquals("\uD83C\uDCA9", Card(Symbol.SPADES, Value.NINE).toString())
        assertEquals("\uD83C\uDCAA", Card(Symbol.SPADES, Value.TEN).toString())
        assertEquals("\uD83C\uDCAB", Card(Symbol.SPADES, Value.JACK).toString())
        assertEquals("\uD83C\uDCAC", Card(Symbol.SPADES, Value.QUEEN).toString())
        assertEquals("\uD83C\uDCAD", Card(Symbol.SPADES, Value.KING).toString())

        assertEquals("\uD83C\uDCB1", Card(Symbol.HEARTS, Value.ACE).toString())
        assertEquals("\uD83C\uDCB2", Card(Symbol.HEARTS, Value.TWO).toString())
        assertEquals("\uD83C\uDCB3", Card(Symbol.HEARTS, Value.THREE).toString())
        assertEquals("\uD83C\uDCB4", Card(Symbol.HEARTS, Value.FOUR).toString())
        assertEquals("\uD83C\uDCB5", Card(Symbol.HEARTS, Value.FIVE).toString())
        assertEquals("\uD83C\uDCB6", Card(Symbol.HEARTS, Value.SIX).toString())
        assertEquals("\uD83C\uDCB7", Card(Symbol.HEARTS, Value.SEVEN).toString())
        assertEquals("\uD83C\uDCB8", Card(Symbol.HEARTS, Value.EIGHT).toString())
        assertEquals("\uD83C\uDCB9", Card(Symbol.HEARTS, Value.NINE).toString())
        assertEquals("\uD83C\uDCBA", Card(Symbol.HEARTS, Value.TEN).toString())
        assertEquals("\uD83C\uDCBB", Card(Symbol.HEARTS, Value.JACK).toString())
        assertEquals("\uD83C\uDCBC", Card(Symbol.HEARTS, Value.QUEEN).toString())
        assertEquals("\uD83C\uDCBD", Card(Symbol.HEARTS, Value.KING).toString())

        assertEquals("\uD83C\uDCC1", Card(Symbol.DIAMONDS, Value.ACE).toString())
        assertEquals("\uD83C\uDCC2", Card(Symbol.DIAMONDS, Value.TWO).toString())
        assertEquals("\uD83C\uDCC3", Card(Symbol.DIAMONDS, Value.THREE).toString())
        assertEquals("\uD83C\uDCC4", Card(Symbol.DIAMONDS, Value.FOUR).toString())
        assertEquals("\uD83C\uDCC5", Card(Symbol.DIAMONDS, Value.FIVE).toString())
        assertEquals("\uD83C\uDCC6", Card(Symbol.DIAMONDS, Value.SIX).toString())
        assertEquals("\uD83C\uDCC7", Card(Symbol.DIAMONDS, Value.SEVEN).toString())
        assertEquals("\uD83C\uDCC8", Card(Symbol.DIAMONDS, Value.EIGHT).toString())
        assertEquals("\uD83C\uDCC9", Card(Symbol.DIAMONDS, Value.NINE).toString())
        assertEquals("\uD83C\uDCCA", Card(Symbol.DIAMONDS, Value.TEN).toString())
        assertEquals("\uD83C\uDCCB", Card(Symbol.DIAMONDS, Value.JACK).toString())
        assertEquals("\uD83C\uDCCC", Card(Symbol.DIAMONDS, Value.QUEEN).toString())
        assertEquals("\uD83C\uDCCD", Card(Symbol.DIAMONDS, Value.KING).toString())

        assertEquals("\uD83C\uDCD1", Card(Symbol.CLUBS, Value.ACE).toString())
        assertEquals("\uD83C\uDCD2", Card(Symbol.CLUBS, Value.TWO).toString())
        assertEquals("\uD83C\uDCD3", Card(Symbol.CLUBS, Value.THREE).toString())
        assertEquals("\uD83C\uDCD4", Card(Symbol.CLUBS, Value.FOUR).toString())
        assertEquals("\uD83C\uDCD5", Card(Symbol.CLUBS, Value.FIVE).toString())
        assertEquals("\uD83C\uDCD6", Card(Symbol.CLUBS, Value.SIX).toString())
        assertEquals("\uD83C\uDCD7", Card(Symbol.CLUBS, Value.SEVEN).toString())
        assertEquals("\uD83C\uDCD8", Card(Symbol.CLUBS, Value.EIGHT).toString())
        assertEquals("\uD83C\uDCD9", Card(Symbol.CLUBS, Value.NINE).toString())
        assertEquals("\uD83C\uDCDA", Card(Symbol.CLUBS, Value.TEN).toString())
        assertEquals("\uD83C\uDCDB", Card(Symbol.CLUBS, Value.JACK).toString())
        assertEquals("\uD83C\uDCDC", Card(Symbol.CLUBS, Value.QUEEN).toString())
        assertEquals("\uD83C\uDCDD", Card(Symbol.CLUBS, Value.KING).toString())


    }
}