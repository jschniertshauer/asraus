package game

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CardDeckTest {

    @Test
    fun creatEmptyDeck() {
        assertTrue( CardDeck.emptyDeck().isEmpty())
    }

    @Test
    fun createDeck() {
        assertFalse(CardDeck.singleDeck().isEmpty())
        assertFalse(CardDeck.doubleDeck().isEmpty())
        assertFalse(CardDeck.tripleDeck().isEmpty())
    }

    @Test
    fun drawEmpty(){
        try {
            CardDeck.emptyDeck().draw()
            fail<String>("cannot draw from empty")
        } catch (e:IndexOutOfBoundsException) {

        }
    }

    @Test
    fun drawFilled(){
        CardDeck.singleDeck().draw()
        CardDeck.doubleDeck().draw()
        CardDeck.tripleDeck().draw()
    }

    @Test
    fun createByPlayers(){
        CardDeck.createDeck(numPlayers=0) // should not crash
        assertFalse(CardDeck.createDeck(1).isEmpty())
        assertFalse(CardDeck.createDeck(2).isEmpty())
        assertFalse(CardDeck.createDeck(3).isEmpty())
        assertFalse(CardDeck.createDeck(4).isEmpty())
    }
}